/********************************************************************
	创建日期:	2013/12/10
	包含文件:	VisionAVTCamera.cpp VisionAVTCamera.h
	  创建者:	文洲
	
		功能:	封装AVT 1394接口相机操作,实现单帧、连续采集和录制视频
				支持多个相机同时使用
		说明:	使用步骤
				1.然后,创建对应的相机对象,Open()相机
				2.AllocImageForShow()分配默认相机采图显示的窗口
				3.剩下既可以自由的单帧、连续采集和录制视频
		注意:	依赖于MIL,确保在new对象前初始化了MIL
*********************************************************************/

#include <StdAfx.h>
#include "VisionHeader.h"
#include "VisionAVTCamera.h"

CVisionAVTCamera::	CVisionAVTCamera(const MIL_ID milApplication, 
									 const MIL_ID milSystem,
									 const double dbDispXZoom/*=1*/, 
									 const double dbDispYZoom/*=1*/,
									 const UINT32 nCameraDeviceID/*=302674458*/,
									 const UINT32 nCameraBufWidth/*=1600*/,
									 const UINT32 nCameraBufHeight/*=1200*/,
									 const UINT8  nCameraRes/*=RES_1600_1200*/,
									 const double dbCcameraFrameRate/*=15*/,
									 const UINT8  nCameraRate/*=FR_15*/,
									 const UINT8  nCameraMode/*=CM_Y8*/)
{
	m_milApplication = milApplication;
	m_milSystem = milSystem;
	m_milDisplay = M_NULL;
	m_milRawImage = M_NULL;
	m_milShowImage = M_NULL;

	m_pRawImage = NULL;

	m_dbDispXZoom = dbDispXZoom;
	m_dbDispYZoom = dbDispYZoom;

	//设置相机采图参数
	m_cameraDeviceID = nCameraDeviceID;		//设置硬件Device ID
	m_cameraWidth = nCameraBufWidth;
	m_cameraHeight = nCameraBufHeight;
	m_cameraRes = nCameraRes;
	m_dbCameraFrameRate = dbCcameraFrameRate;
	m_cameraRate = nCameraRate;
	m_cameraMode = nCameraMode;

	//Flag参数
	m_bIsOpen = FALSE;
	m_bIsStartGrab = FALSE;

	m_nRecordCount = 0;
}

CVisionAVTCamera::~CVisionAVTCamera()
{
	Close();
}

BOOL CVisionAVTCamera::Open()
{
	UINT32 Result;
	UINT32 NodeCnt = 0;

	//如果打开先关闭
	Close();

	//遍历选择连接对应device id的相机
	Result = FGGetNodeList(m_FGNodeInfo, AVT_CAMERA_MAXNUM, &NodeCnt);	
	for (int i=0; i<AVT_CAMERA_MAXNUM; i++)
	{
		if (m_FGNodeInfo[i].Guid.Low == m_cameraDeviceID)
		{
			Result = m_FGCamera.Connect(&m_FGNodeInfo[i].Guid, (void*)(this));//当前对象指针当做参数传递
			break;
		}
	}

	if(Result==FCE_NOERROR)
	{
		//设置相机参数
		m_FGCamera.SetParameter(FGP_TRIGGER, MAKETRIGGER(0, 0, 0, 0, 0));
		m_FGCamera.SetParameter(FGP_IMAGEFORMAT, MAKEIMAGEFORMAT(m_cameraRes, m_cameraMode, m_cameraRate));

		//分配采图内存
		m_pRawImage = (BYTE *)malloc(sizeof(BYTE)*m_cameraWidth*m_cameraHeight);
		MbufAlloc2d(m_milSystem, m_cameraWidth, m_cameraHeight, 8+M_UNSIGNED, M_IMAGE+M_PROC, &m_milRawImage);
		MbufAlloc2d(m_milSystem, m_cameraWidth, m_cameraHeight, 8+M_UNSIGNED, M_IMAGE+M_PROC+M_DISP, &m_milShowImage);
		MdispAlloc(m_milSystem, M_DEFAULT, "M_DEFAULT", M_WINDOWED, &m_milDisplay);

		return m_bIsOpen = TRUE;
	}
	else
	{
		return FALSE;
	}
}

void CVisionAVTCamera::Close()
{
	//停止采集
	if (m_bIsStartGrab == TRUE)
	{
		m_FGCamera.StopDevice();
		m_FGCamera.CloseCapture();

		m_bIsStartGrab = FALSE;
	}
	
	
	if (m_bIsOpen == TRUE)
	{
		//删除对象
		if (NULL != m_pRawImage)
		{
			free(m_pRawImage);
		}
		if (M_NULL != m_milRawImage)
		{
			MbufFree(m_milRawImage);
		}
		if (M_NULL != m_milShowImage)
		{
			MbufFree(m_milShowImage);
		}
		if (M_NULL != m_milDisplay)
		{
			MbufFree(m_milDisplay);
		}

		//释放连接
		m_FGCamera.Disconnect();

		m_bIsOpen = FALSE;
	}
}

void CVisionAVTCamera::AllocImageForShow(const HWND hwnd )
{
	MdispZoom(m_milDisplay, m_dbDispXZoom, m_dbDispYZoom);
	MbufClear(m_milShowImage, 0x0);
	MdispControl(m_milDisplay, M_OVERLAY_CLEAR, M_DEFAULT);
	MdispSelectWindow(m_milDisplay, m_milShowImage, hwnd);
}

void CVisionAVTCamera::FreeImageForShow()
{
	MbufClear(m_milShowImage, 0x0);
	MdispSelectWindow(m_milDisplay, NULL, NULL); 
}

void CVisionAVTCamera::SingleGrab()
{
	if (m_bIsStartGrab == TRUE)
	{
		m_FGCamera.StopDevice();
		m_FGCamera.CloseCapture();

		m_bIsStartGrab = FALSE;
	}

	//触发一次单帧采图
	m_grabMode = SINGLE_GRAB;
	m_bIsStartGrab = TRUE;
	
	m_FGCamera.OpenCapture();
	m_FGCamera.StartDevice();
}

void CVisionAVTCamera::StartContinueGrab()
{
	if (m_bIsStartGrab == TRUE)
	{
		m_FGCamera.StopDevice();
		m_FGCamera.CloseCapture();

		m_bIsStartGrab = FALSE;
	}

	//触发连续采图
	m_grabMode = CONTINUE_GRAB;
	m_bIsStartGrab = TRUE;
	
	m_FGCamera.OpenCapture();
	m_FGCamera.StartDevice();
}

void CVisionAVTCamera::StopContonueGrab()
{
	m_FGCamera.StopDevice();
	m_FGCamera.CloseCapture();
	
	m_bIsStartGrab = FALSE;
}

void CVisionAVTCamera::StartRecordGrab( PCSTR szFileName )
{
	if (m_bIsStartGrab == TRUE)
	{
		m_FGCamera.StopDevice();
		m_FGCamera.CloseCapture();

		m_bIsStartGrab = FALSE;
	}

	//触发连续采图同时录制
	m_grabMode = RECORD_GRAB;
	m_bIsStartGrab = TRUE;
	lstrcpy(m_szRecordFile, szFileName);
	m_nRecordCount = 0;

	//打开文件
	MbufExportSequence(m_szRecordFile, M_AVI_DIB, M_NULL, M_NULL, M_NULL, M_OPEN);
	
	m_FGCamera.OpenCapture();
	m_FGCamera.StartDevice();
}

void CVisionAVTCamera::StopRecordGrab()
{
	m_FGCamera.StopDevice();
	m_FGCamera.CloseCapture();
	
	m_bIsStartGrab = FALSE;

	//关闭文件
	MbufExportSequence(m_szRecordFile, M_AVI_DIB, M_NULL, M_NULL, M_NULL, M_CLOSE);
}

void  WINAPI CVisionAVTCamera::CallBackCaptureImage( void* Context, UINT32 wParam, void* lParam )
{
	char   Text[20]= {'\0'};
	
	switch (wParam)
	{
	case WPARAM_FRAMESREADY:
		//获得当前处理的相机对象
		CVisionAVTCamera *pCamera = (CVisionAVTCamera *)(lParam);
		
		if (pCamera->m_bIsStartGrab == FALSE || pCamera->m_bIsOpen == FALSE)
		{
			return;
		}
		
		//采集图像到相机Raw Mil Buffer中
		pCamera->CaptureImage(pCamera->m_pRawImage);
		MbufPut(pCamera->m_milRawImage, pCamera->m_pRawImage);
		
		//注意::不同的采图模式做不同处理,如果想对不同的相机做不同处理也可以在此根据device id判断扩展
		switch (pCamera->m_grabMode)
		{
		case SINGLE_GRAB:
			MbufCopy(pCamera->m_milRawImage, pCamera->m_milShowImage);//显示
			break;
		case CONTINUE_GRAB:
			MbufCopy(pCamera->m_milRawImage, pCamera->m_milShowImage);//显示
			break;
		case RECORD_GRAB:
			//将当前采图计数写到采到图上
			pCamera->m_nRecordCount++;//采图计数增加
			MOs_ltoa(pCamera->m_nRecordCount, Text, 10);
			MgraText(M_DEFAULT, pCamera->m_milRawImage, 10, 10, Text);
			
			//采图写到对应文件中
			MbufExportSequence(pCamera->m_szRecordFile, M_AVI_DIB, &(pCamera->m_milRawImage), 1, 
				               pCamera->m_dbCameraFrameRate, M_WRITE);
			
			//显示
			MbufCopy(pCamera->m_milRawImage, pCamera->m_milShowImage);
			break;
		}
		
		break;
	}
}

void CVisionAVTCamera::CaptureImage( BYTE *pImageBuffer)
{
	FGFRAME   Frame;
	UINT32    Result;
	
	BYTE *pData1 = NULL;
	BYTE *pData2 = NULL;
	
	UINT32 iWidth = m_cameraWidth;
	UINT32 iHeight = m_cameraHeight;
	
	//这段代码太乱,没敢大改
	if (TRUE == m_bIsStartGrab && SINGLE_GRAB != m_grabMode) 
	{
		do
		{	// Try to read a frame
			Result=m_FGCamera.GetFrame(&Frame,0);			
			// Only if <pData> is valid (ignore pure frame start events)
			
			if(Frame.pData)
			{
				pData1 = pImageBuffer;
				pData2 = Frame.pData+iWidth*(iHeight-1);

				bool bflag = false;//false 不分奇偶场；true 分奇遇场
				if(bflag)
				{
					UINT32 i;
					for(i=0;i<iHeight/2;i++)
					{						
						// 拷贝偶场
						memcpy(pData1, pData2-iWidth*iHeight/2, iWidth);
						// 拷贝奇场
						memcpy(pData1+iWidth, pData2, iWidth);	
						// 目标地址移动奇偶场各一行
						pData1 += iWidth*2;	
						// 源地址移动一行,以偶场为基准
						pData2 -= iWidth;							
					}
				}
				else
				{
					for(UINT32 i=0;i<iHeight;i++)
					{
						memcpy(pData1+iWidth*i, pData2-iWidth*i, iWidth);
					}	
				}
				
				m_FGCamera.PutFrame(&Frame);
			}
		}while(!(Frame.Flags&FGF_LAST));
	}
	else if (TRUE == m_bIsStartGrab && SINGLE_GRAB == m_grabMode)
	{
		// Wait 5000 MilliSeconds for a frame
		Result=m_FGCamera.GetFrame(&Frame,500);
		if(Result==FCE_NOERROR)
		{
			Result=m_FGCamera.PutFrame(&Frame);
		}
		if(Frame.pData)
		{
			pData1 = pImageBuffer;
			pData2 = Frame.pData+iWidth*(iHeight-1);
			for(UINT32 i=0;i<iHeight;i++)
			{
				memcpy(pData1+iWidth*i, pData2-iWidth*i, iWidth);
			}
			m_FGCamera.PutFrame(&Frame);
		}
		
		m_FGCamera.StopDevice();
		m_FGCamera.CloseCapture();
		m_bIsStartGrab = FALSE;
	}
}

MIL_ID CVisionAVTCamera::GetCaptureImageID()
{
	return m_milRawImage;
}



