#ifndef VISIONMILCAMERA_H_H
#define VISIONMILCAMERA_H_H

//MIL采集Buffer List个数
#define BUFFERING_SIZE_MAX 30

class CVisionMILCamera;//先声明类

//视屏录制参数
typedef struct
{
	MIL_ID  MilImageDisp;
	long    ProcessedImageCount;
	MIL_ID	MilDigtizer;
	TCHAR	szRecordFile[MAX_PATH];
} HookDataStruct;

class VISION_DLL CVisionMILCamera  
{
public:
	CVisionMILCamera(const MIL_ID milApplication, const MIL_ID milSystem, 
					 const double dbDispXZoom=1, const double dbDispYZoom=1,
					 const double dbGrabXScale=1, const double dbGrabYScale=1);
	virtual ~CVisionMILCamera();
	
	BOOL Open();
	void Close();
	void AllocImageForShow(const HWND hwnd );
	void FreeImageForShow();
	void SingleGrab();
	void StartContinueGrab();
	void StopContonueGrab();
	void StartRecordGrab( LPCSTR szFileName );
	void StopRecordGrab();

	MIL_ID GetCaptureImageID();

private:
	//MIL参数
	MIL_ID  m_milApplication;
	MIL_ID  m_milSystem;
	MIL_ID	m_milDigtizer;
	MIL_ID  m_milDisplay ;
	MIL_ID	m_milRawImage;
	MIL_ID	m_milShowImage;

	double	m_dbDispXZoom;
	double	m_dbDispYZoom;
	double	m_dbGrabXScale;
	double	m_dbGrabYScale;

	//相机参数
	long	m_bufSizeX;
	long	m_bufSizeY;
	long	m_bufSizeBand;
	long	m_bufType;
	long	m_bufLocation;

	//Flag参数
	BOOL	m_bIsOpen;
	BOOL	m_bIsContinue;
	BOOL	m_bIsRecord;

	//录制视频参数
	MIL_ID				m_MilGrabBufferList[BUFFERING_SIZE_MAX];		//多Buffer采集时的Buffer List
	long				m_MilGrabBufferListSize;						//实际分配的Buffer个数
	TCHAR				m_szRecordFile[MAX_PATH];						//录制视频保存的全路径
};

#endif
