#ifndef VISIONSYSTEM_H_H
#define VISIONSYSTEM_H_H

#include "VisionHeader.h"
#include "VisionAVTCamera.h"
#include "VisionMILCamera.h"

class VISION_DLL CVisionSystem  
{
public:
	CVisionSystem();
	virtual ~CVisionSystem();
	BOOL InitSystem();
	CVisionMILCamera *GetJetCamera();
	CVisionAVTCamera *GetSubstrateCamera();
	
private:
	//Flag参数
	BOOL m_bInit;

	//MIL参数
	MIL_ID m_milApplication;
	MIL_ID m_milSystem;

	//相机对象
	CVisionAVTCamera *m_pCamaraSubstrate;
	CVisionMILCamera *m_pCameraJet;
};

#endif