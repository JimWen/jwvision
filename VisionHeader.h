#ifndef VISIONHEADER_H_H
#define VISIONHEADER_H_H

// #ifdef VISIONSYSTEM_EXPORTS
// #define VISION_DLL __declspec(dllexport)
// #else
// #define VISION_DLL __declspec(dllimport)
// #endif

#define VISION_DLL

/***函数库引入***/
//引入MIL库
#include <Mil.h>
#pragma comment(lib, "mil.lib")

//引入FireGrab库
#include <FGCamera.h>
#pragma comment(lib, "FGCamera.lib")

/***AVT相机***/
#define AVT_CAMERA_MAXNUM				2			//AVT相机个数

//AVT 基板相机
#define SUB_DISP_X_ZOOM					0.5
#define SUB_DISP_Y_ZOOM					0.5
#define SUB_DEVICE_ID					302671036
#define SUB_BUF_WIDTH					1600
#define SUB_BUF_HEIGHT					1200
#define SUB_RES							RES_1600_1200
#define SUB_FRAME_RATE					15
#define SUB_RATE						FR_15
#define SUB_MODE						CM_Y8

/***MIL 射流观测相机***/
#define JET_GRAB_X_SCALE				(1.0)
#define JET_GRAB_Y_SCALE				(1.0)
#define JET_DISP_X_ZOOM					0.5
#define JET_DISP_Y_ZOOM					0.5
						
#endif
