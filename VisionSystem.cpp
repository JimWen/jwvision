#include <StdAfx.h>
#include "VisionSystem.h"

CVisionSystem::CVisionSystem()
{
	m_bInit = FALSE;

	m_milApplication = M_NULL;
	m_milSystem = M_NULL;

	m_pCamaraSubstrate = NULL;
	m_pCameraJet = NULL;
}

CVisionSystem::~CVisionSystem()
{	
	if (TRUE == m_bInit)
	{
		m_pCameraJet->Close();
		m_pCamaraSubstrate->Close();
	}

	//删除相机对象
	if (NULL != m_pCameraJet)
	{
		delete m_pCameraJet;
		m_pCameraJet = NULL;
	}
	if (NULL != m_pCamaraSubstrate)
	{
		delete m_pCamaraSubstrate;
		m_pCamaraSubstrate = NULL;
	}

	//卸载AVT模块
	FGExitModule();

	//释放MIL资源
	if (M_NULL != m_milApplication)
	{
		MappFreeDefault(m_milApplication, m_milSystem, M_NULL, M_NULL, M_NULL);
		m_milApplication = M_NULL;
		m_milSystem = M_NULL;
	}

	m_bInit = FALSE;
}

CVisionMILCamera * CVisionSystem::GetJetCamera()
{
	return m_pCameraJet;
}

CVisionAVTCamera * CVisionSystem::GetSubstrateCamera()
{
	return m_pCamaraSubstrate;
}

BOOL CVisionSystem::InitSystem()
{
	/***如果已经初始化则...***/
	if (TRUE == m_bInit)
	{
		m_pCameraJet->Close();
		m_pCamaraSubstrate->Close();
	}
		
	//删除相机对象
	if (NULL != m_pCameraJet)
	{
		delete m_pCameraJet;
		m_pCameraJet = NULL;
	}
	if (NULL != m_pCamaraSubstrate)
	{
		delete m_pCamaraSubstrate;
		m_pCamaraSubstrate = NULL;
	}
	
	//卸载AVT模块
	FGExitModule();
	
	//释放MIL资源
	if (M_NULL != m_milApplication)
	{
		MappFreeDefault(m_milApplication, m_milSystem, M_NULL, M_NULL, M_NULL);
		m_milApplication = M_NULL;
		m_milSystem = M_NULL;
	}
	
	m_bInit = FALSE;

	/*********************************************************************************/
	//初始化MIL系统
	MappAllocDefault(M_SETUP, &m_milApplication, &m_milSystem, M_NULL, M_NULL, M_NULL);
	if (M_NULL_ERROR != MappGetError(M_GLOBAL, M_NULL))
	{
		return FALSE;
	}

	//初始化AVT函数库
	FGINIT  Arg;
	UINT32 Result;
	memset(&Arg, 0, sizeof(FGINIT));
	Arg.pCallback = CVisionAVTCamera::CallBackCaptureImage;
	Arg.Context = NULL;
	Result=FGInitModule(&Arg);

	if (Result != FCE_NOERROR)
	{
		return FALSE;
	}

	//初始化对象
	m_pCamaraSubstrate = new CVisionAVTCamera(m_milApplication, 
											  m_milSystem,
											  SUB_DISP_X_ZOOM,
											  SUB_DISP_Y_ZOOM,
											  SUB_DEVICE_ID,
											  SUB_BUF_WIDTH,
											  SUB_BUF_HEIGHT,
											  SUB_RES,
											  SUB_FRAME_RATE,
											  SUB_RATE,
											  SUB_MODE);
	m_pCameraJet = new CVisionMILCamera(m_milApplication,
										m_milSystem,
										JET_DISP_X_ZOOM,
										JET_DISP_Y_ZOOM,
										JET_GRAB_X_SCALE,
										JET_GRAB_Y_SCALE);
	
	//打开AVT 基板相机
	if (FALSE == m_pCamaraSubstrate->Open())
	{
		return FALSE;
	}

	//打开MIL 射流相机
	if (FALSE == m_pCameraJet->Open())
	{
		return FALSE;
	}
	
	return m_bInit = TRUE;
}
