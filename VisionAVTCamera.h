// VisionAVTDevice.h: interface for the CVisionAVTCamera class.
//
//////////////////////////////////////////////////////////////////////
#ifndef VISIONAVTCAMERA_H_H
#define VISIONAVTCAMERA_H_H

typedef enum{
	SINGLE_GRAB,
	CONTINUE_GRAB,
	RECORD_GRAB
}AVT_GRAB_MODE;

class VISION_DLL CVisionAVTCamera  
{
public:
	CVisionAVTCamera(const MIL_ID milApplication, 
					 const MIL_ID milSystem,
					 const double dbDispXZoom=1, 
					 const double dbDispYZoom=1,
					 const UINT32 nCameraDeviceID=302674458,
					 const UINT32 nCameraBufWidth=1600,
					 const UINT32 nCameraBufHeight=1200,
					 const UINT8  nCameraRes=RES_1600_1200,
					 const double dbCcameraFrameRate=15,
					 const UINT8  nCameraRate=FR_15,
					 const UINT8  nCameraMode=CM_Y8
					 );
	virtual ~CVisionAVTCamera();

	BOOL Open();
	void Close();
	void AllocImageForShow(const HWND hwnd );
	void FreeImageForShow();
	void SingleGrab();
	void StartContinueGrab();
	void StopContonueGrab();
	void StartRecordGrab( PCSTR szFileName );
	void StopRecordGrab();

	MIL_ID GetCaptureImageID();

	static	void  WINAPI CallBackCaptureImage(void* Context, UINT32 wParam, void* lParam);

private:
	CFGCamera m_FGCamera;
	FGNODEINFO m_FGNodeInfo[AVT_CAMERA_MAXNUM];
	void CaptureImage( BYTE *pImageBuffer);
public:
	//MIL参数
	MIL_ID m_milApplication;
	MIL_ID m_milSystem;
	MIL_ID m_milDisplay;
	MIL_ID m_milRawImage;
	MIL_ID m_milShowImage;

	BYTE *m_pRawImage;

	double	m_dbDispXZoom;
	double	m_dbDispYZoom;

	//相机参数
	UINT32 m_cameraDeviceID;
	UINT32 m_cameraWidth;
	UINT32 m_cameraHeight;
	UINT8  m_cameraRes;
	double m_dbCameraFrameRate;
	UINT8  m_cameraRate;
	UINT8  m_cameraMode;

	//Flag参数
	BOOL m_bIsOpen;
	BOOL m_bIsStartGrab;
	AVT_GRAB_MODE m_grabMode;

	//录制视频参数
	TCHAR m_szRecordFile[MAX_PATH];
	long m_nRecordCount;
};

#endif